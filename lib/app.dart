import 'package:flutter/material.dart';
import 'package:validators/validators.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login me',
      home: Scaffold(
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}



class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String email;
  late String name;
  late String lastmiddlename;
  late String birthyear;
  late String address;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(20.0),
      child: Form(
        key: formKey,
        child: Column(
          children: [
            emailField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            middlelastField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            nameField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            birthyearField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            addressField(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            loginButton()
          ],
        ),
      ),
    );
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
          labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@')) {
          return "Input valid email.";
        }
        return null;
      },

      onSaved: (value) {
        email = value as String;
      },

    );
  }

  Widget middlelastField() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Middle and last name'
      ),
    validator: (value) {
        if(value!.isEmpty == true) {
          return "Please enter middle and last name!";
        }
        return null;
    },

      onSaved: (value){
        lastmiddlename = value as String;
      }
    );
  }

  Widget nameField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'Name'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter name!";
          }
          return null;
        },

        onSaved: (value){
          name = value as String;
        }
    );
  }

  Widget birthyearField() {
    return TextFormField(
        keyboardType: TextInputType.number,
        decoration: InputDecoration(

            labelText: 'Birthyear'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter birthyear!";
          }
          return null;
        },

        onSaved: (value){
          birthyear = value as String;
        }
    );
  }

  Widget addressField() {
    return TextFormField(
        decoration: InputDecoration(
            labelText: 'Address'
        ),
        validator: (value) {
          if(value!.isEmpty == true) {
            return "Please enter address!";
          }
          return null;
        },

        onSaved: (value){
          address = value as String;
        }
    );
  }
  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            formKey.currentState!.save();

            print('email=$email');
          }
        },
        child: Text('Login')
    );
  }
}